package com.rw.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSimpleDataSource {

	ApplicationContext context;

	@Before
	public void init() {
		if (null == context) {
			context = new ClassPathXmlApplicationContext(
					"classpath:spring-SimpleDataSource.xml");
		}
	}

	@Test
	public void test() {
		DataSource dataSource = (DataSource) context.getBean("dataSource");
		try {
			Connection con = dataSource.getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT 1");
			if (rs.next()) {
				assertEquals(rs.getInt(1), 1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
