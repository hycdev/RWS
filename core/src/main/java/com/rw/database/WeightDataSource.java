/**
 * 
 */
package com.rw.database;

/**
 * 权重数据源
 * 
 * @author lkclkc88(lkclkc88@sina.com)
 * @Date 2016年1月29日
 */
public class WeightDataSource extends DataSourceAdapter {
	/**
	 * 权重
	 */
	private int weight;

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
