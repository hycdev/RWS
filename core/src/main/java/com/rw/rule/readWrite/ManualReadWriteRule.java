package com.rw.rule.readWrite;

import com.rw.database.ReadWriteDataSource;
import com.rw.rule.ReadWriteRule;

/**
 * 手动实现读写分离规则，使用threadLocal实现，设置之后，在当前线程中会一直保持读写状态，直到调用方法修改读写状态
 * 
 * @author lkclkc88(lkclkc88@sina.com)
 * @Date 2016年1月27日
 */

public class ManualReadWriteRule implements ReadWriteRule {

	ThreadLocal<Object> local = new ThreadLocal<Object>();

	@Override
	public boolean isRead(ReadWriteDataSource dataSource) {
		return local.get() != null;
	}

	/**
	 * 设置为读
	 */
	public void setRead() {
		local.remove();
	}

	/**
	 * 设置为写
	 */
	public void setWrite() {
		local.set(1);
	}
}
