package com.rw.rule.readWrite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rw.database.ReadWriteDataSource;
import com.rw.rule.ReadWriteRule;



/**
 * 默认规则，默认返回false
 * 
 * @author lkclkc88(lkclkc88@sina.com)
 * @Date 2016年1月27日
 */
public class DefaultReadWriteRule implements ReadWriteRule {
	
	private static Logger log=LoggerFactory.getLogger(DefaultReadWriteRule.class);

	@Override
	public boolean isRead(ReadWriteDataSource dataSource) {
		log.debug("isRead = {}",true);
		return true;
	}

}
