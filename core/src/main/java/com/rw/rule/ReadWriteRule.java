package com.rw.rule;

import com.rw.database.ReadWriteDataSource;

/**
 * 读写规则，用于决定数据源采用读或者写
 * 
 * @author lkclkc88(lkclkc88@sina.com)
 * @Date 2016年1月27日
 */
public interface ReadWriteRule {
	/**
	 * 是否为读，如果返回true，采用读数据源，否者采用写数据源
	 * 
	 * @param dataSource
	 * @return
	 */
	public boolean isRead(ReadWriteDataSource dataSource);

}
