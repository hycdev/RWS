package com.rw.rule;

import java.util.List;

import javax.sql.DataSource;

/**
 * 数据源规则，用于从数据源集合中采用合理的数据源
 * 
 * @author lkclkc88(lkclkc88@sina.com)
 * @Date 2016年1月27日
 */
public interface LoadRule {
	/**
	 * 执行规则，返回数据源,如果读数据源为小于等于1时，不会调用次接口
	 * 
	 * @param list
	 * @return
	 */
	DataSource process(List<DataSource> list);

	/**
	 * 初始化规则
	 * 
	 * @param list
	 */
	void initRule(List<DataSource> list);
}
